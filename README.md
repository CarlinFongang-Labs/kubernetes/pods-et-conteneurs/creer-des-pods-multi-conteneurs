# Créer des pods multi-conteneurs



------------


><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______


### Création de Pods Multi-conteneurs

#### Introduction
Dans cette leçon, nous allons parler de la création de pods multi-conteneurs. Tout d'abord, nous allons expliquer ce qu'est un pod multi-conteneurs. Ensuite, nous parlerons de l'interaction entre conteneurs. Nous discuterons d'un cas d'utilisation et nous ferons une brève démonstration pratique.

#### Qu'est-ce qu'un Pod Multi-Conteneurs ?
Un pod Kubernetes peut avoir un ou plusieurs conteneurs. Ainsi, tout pod avec plus d'un conteneur est considéré comme un pod multi-conteneurs. Dans un pod multi-conteneurs, les conteneurs partagent des ressources telles que le réseau et le stockage, ce qui leur permet d'interagir les uns avec les autres pour fournir des fonctionnalités.

#### Meilleures Pratiques
Il est recommandé de garder les conteneurs dans des pods séparés, sauf s'il est vraiment nécessaire de créer un pod multi-conteneurs. À moins que ces conteneurs aient besoin d'être étroitement couplés et de partager des ressources, il est préférable de les garder dans des pods séparés. Cependant, s'ils doivent travailler ensemble pour fournir des fonctionnalités supplémentaires, les pods multi-conteneurs sont une excellente solution.

#### Interaction entre Conteneurs
Une fois que nous avons plusieurs conteneurs dans le même pod, comment interagissent-ils entre eux ? Il existe des moyens spécifiques pour que les conteneurs partageant un pod interagissent entre eux, appelés interaction entre conteneurs. Les conteneurs partageant le même pod peuvent interagir en utilisant les ressources partagées au sein de ce pod.

#### Exemple de Cas d'Utilisation
Prenons un exemple concret. Supposons que vous ayez une application qui écrit les journaux dans un fichier sur le disque dur. La plupart des conteneurs écrivent leurs journaux directement dans la console, et ces journaux se retrouvent dans les logs du conteneur, accessibles via une commande comme `kubectl logs`. Cependant, si l'application est configurée pour écrire les journaux dans un fichier, vous ne pourrez pas accéder facilement à ces journaux. Pour résoudre ce problème, vous pouvez utiliser un pod multi-conteneurs en ajoutant un conteneur secondaire, souvent appelé sidecar. Ce conteneur secondaire peut lire le fichier de journaux à partir d'un volume partagé et l'afficher dans la console, permettant ainsi d'accéder aux journaux.

### Manifeste pour un Pod multi-conteneurs simple

Voici le manifeste YAML pour un pod multi-conteneurs avec nginx, redis et couchbase :

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: multi-container-pod
spec:
  containers:
  - name: nginx
    image: nginx
  - name: redis
    image: redis
  - name: couchbase
    image: couchbase
```

### Manifeste pour un Pod avec **conteneur sidecar**

Voici le manifeste YAML pour un pod avec un **conteneur sidecar** :

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: sidecar-pod
spec:
  containers:
  - name: busyboy1
    image: busybox
    command: ["/bin/sh", "-c", "while true; do echo 'log data' >> /output/output.log; sleep 5; done"]
    volumeMounts:
    - name: shared-vol
      mountPath: /output
  - name: sidecar
    image: busybox
    command: ["/bin/sh", "-c", "tail -f /input/output.log"]
    volumeMounts:
    - name: shared-vol
      mountPath: /input
  volumes:
  - name: shared-vol
    emptyDir: {}
```

### Commandes Kubernetes

Pour créer les pods à partir des fichiers YAML, utilisez les commandes suivantes :

1. Créez le pod multi-conteneurs simple :

```bash
kubectl apply -f multi-container-pod.yml
```

2. Créez le pod avec le conteneur sidecar :

```bash
kubectl apply -f sidecar-pod.yml
```

3. Vérifiez les journaux du conteneur sidecar :

```sh
kubectl logs sidecar-pod -c sidecar
```

### Résumé de la Leçon

Dans cette leçon, nous avons parlé des pods multi-conteneurs, de l'interaction entre conteneurs, d'un cas d'utilisation concret et nous avons fait une démonstration pratique. C'est tout pour cette leçon. À la prochaine !



# Reférence



https://kubernetes.io/docs/concepts/workloads/pods/#using-pods

https://kubernetes.io/docs/tasks/access-application-cluster/communicate-containers-same-pod-shared-volume/

https://kubernetes.io/blog/2015/06/the-distributed-system-toolkit-patterns/

